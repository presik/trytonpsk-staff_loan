# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool

from . import account, configuration, loan, voucher


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationDefaultAccount,
        configuration.Sequence,
        loan.Loan,
        loan.LoanLine,
        account.Move,
        account.MoveLine,
        loan.LoanPaymentLine,
        loan.DayPayment,
        voucher.Voucher,
        module='staff_loan', type_='model')
    Pool.register(
        loan.LoanReport,
        module='staff_loan', type_='report')
    Pool.register(
        module='staff_loan', type_='wizard')
