# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import calendar
from collections import defaultdict
from datetime import date
from decimal import Decimal

from dateutil.relativedelta import relativedelta
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Unique, Workflow, fields
from trytond.model.exceptions import AccessError
from trytond.modules.company.model import employee_field, reset_employee, set_employee
from trytond.pool import Pool
from trytond.pyson import Bool, Eval, If
from trytond.report import Report
from trytond.tools import grouped_slice, reduce_ids
from trytond.transaction import Transaction

from .exceptions import LoanMissingSequence

STATES = {'readonly': (Eval('state') != 'draft')}
_ZERO = Decimal('0.0')


class Loan(Workflow, ModelSQL, ModelView):
    "Loan"
    __name__ = 'staff.loan'

    number = fields.Char('Number', readonly=True, help="Secuence",
                         )
    party = fields.Many2One('party.party', 'Party',
                               states=STATES, required=True, depends=['state'])
    date_effective = fields.Date('Date Effective', states=STATES,
                                 required=True)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('approved', 'Approved'),
            ('processed', 'Processed'),
            ('posted', 'Posted'),
            ('cancelled', 'Cancel'),
            ('paid', 'Paid'),
            ], 'State', readonly=True)
    state_string = state.translated('state')

    company = fields.Many2One('company.company', 'Company', required=True,
        states={
            'readonly': (Eval('state') != 'draft') | Eval('lines', [0]),
            },
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
            Eval('context', {}).get('company', 0)),
            ],
        depends=['state'])
    description = fields.Char('Description')
    lines = fields.One2Many('staff.loan.line', 'loan', 'Lines')
    amount = fields.Numeric('Total Amount', digits=(16, 2),
        states={'required': True})
    number_instalment = fields.Integer('Number Instalment',
        states={'required': True})
    approved_by = employee_field(
        "Approved By",
        states=['processed', 'done', 'cancelled'])
    processed_by = employee_field(
        "Processed By",
        states=['processed', 'done', 'cancelled'])
    comment = fields.Text('Comment')
    currency = fields.Many2One('currency.currency', 'Currency', required=True)
    account_move = fields.Many2One('account.move', 'Account Move', readonly=True)
    account = fields.Many2One('account.account', 'Account',
        states={
            'required': Eval('state').in_(['processed', 'posted']),
            'readonly': Eval('state') != 'draft',
        },
        domain=[
            ('closed', '!=', True),
            ('company', '=', Eval('company', -1)),
            ('type', '!=', None),
            ])

    reconciled = fields.Function(fields.Date('Reconciled',
            states={
                'invisible': ~Eval('reconciled'),
                }),
            'get_reconciled')
    type = fields.Selection([
            ('external', 'External'),
            ('in_company', 'In Company'),
        ], 'Type', states={
            'required': True,
            'readonly': Eval('state') != 'draft',
        })
    payment_mode = fields.Many2One('account.voucher.paymode', 'Payment Mode',
        states={
            'required': Eval('type') != 'external',
            'readonly': Eval('state') != 'draft',
        })

    payment_lines = fields.Many2Many('staff.loan-account.move.line',
        'loan', 'line', string='Payment Lines',
        domain=[
            ('account', '=', Eval('account', -1)),
            ('party', 'in', [None, Eval('party', -1)]),
            If(Eval('amount', 0) >= 0,
                ('debit', '=', 0),
                ('credit', '=', 0)),
            ['OR',
                ('loan_payment', '=', None),
                ('loan_payment', '=', Eval('id', -1)),
            ],
            ],
        states={
            'readonly': Eval('state') != 'posted',
            },
        depends=['state', 'account', 'party', 'id', 'total_amount'])

    lines_to_pay = fields.Function(fields.Many2Many(
            'account.move.line', None, None, 'Lines to Pay'),
        'get_lines_to_pay')

    amount_to_pay = fields.Function(fields.Numeric('Amount to Pay',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']), 'get_amount_to_pay')

    party_to_pay = fields.Many2One('party.party', 'Party to pay', states={
                'readonly': Eval('state') != 'draft',
                'required': Eval('type') == 'external',
                'invisible': Eval('type') != 'external',
            }, depends=['state', 'type'])
    wage_type = fields.Many2One('staff.wage_type', 'Wage Type')
    days_of_month = fields.One2Many('staff_loan.day_payment', 'loan', 'Days of Month')
    first_pay_date = fields.Date('First Pay Date', states={'required': True})

    @classmethod
    def __setup__(cls):
        super(Loan, cls).__setup__()
        cls._order.insert(0, ('date_effective', 'ASC'))
        cls._transitions |= set((
                ('draft', 'approved'),
                ('approved', 'processed'),
                ('processed', 'posted'),
                ('processed', 'draft'),
                ('posted', 'paid'),
                ('draft', 'cancelled'),
                ('approved', 'cancelled'),
                ('approved', 'draft'),
                ('cancelled', 'draft'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': ~Eval('state').in_(['draft', 'done']),
                    'depends': ['state'],
                    },
                'draft': {
                    'invisible': ~Eval('state').in_(
                        ['cancelled', 'approved']),
                    'icon': If(Eval('state') == 'cancelled',
                        'tryton-undo',
                        'tryton-back'),
                    'depends': ['state'],
                    },
                'approve': {
                    'invisible':  Bool(~Eval('lines', [])) | Bool(Eval('state') != 'draft'),
                    'depends': ['state', 'lines'],
                    },
                'process': {
                    'invisible': Eval('state') != 'approved',
                    'icon': 'tryton-forward',
                    'depends': ['state'],
                    },
                'post': {
                    'invisible': Eval('state') != 'processed',
                    'icon': 'tryton-forward',
                    'depends': ['state'],
                    },
                'refresh': {
                    'invisible': Eval('state') == 'draft',
                    'icon': 'tryton-refresh',
                    'depends': ['state'],
                    },
                'calculate': {
                    'invisible': Eval('state') != 'draft',
                    'icon': 'tryton-refresh',
                    'depends': ['state'],
                    },
                })

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        if table_h.column_exist('account_debit'):
            table_h.column_rename('account_debit', 'account')
        super().__register__(module_name)

    @staticmethod
    def default_currency():
        pool = Pool()
        Company = pool.get('company.company')
        company = Transaction().context.get('company')
        if company:
            return Company(company).currency.id

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('wage_type', 'account')
    def on_change_wage_type(self, name=None):
        if self.wage_type:
            self.account = self.wage_type.credit_account
        else:
            self.account = None

    @staticmethod
    def default_account():
        pool = Pool()
        Configuration = pool.get('loan.configuration')
        configuration = Configuration(1)
        company = Transaction().context.get('company')
        account = configuration.get_multivalue(
                    'default_account_debit', company=company)
        if account:
            return account.id

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, loans):
        for loan in loans:
            if loan.state == 'posted':
                raise AccessError(gettext(
                    'staff_loan.msg_dont_cancel_loan', loan=loan.rec_name))

    @classmethod
    @ModelView.button
    def refresh(cls, loans):
        paid = []
        posted = []
        for loan in loans:
            if loan.state not in ('posted', 'paid'):
                continue
            if loan.reconciled:
                paid.append(loan)
            else:
                posted.append(loan)
        cls.validate_loan(loans)
        cls.paid(paid)
        cls.post(posted)

    @classmethod
    @Workflow.transition('paid')
    def paid(cls, loans):
        # Remove links to lines which actually do not pay the loan
        cls._clean_payments(loans)

    @classmethod
    def validate_loan(cls, loans):
        lines = []
        for loan in loans:
            lines.extend(loan.lines)
        pool = Pool()
        LoanLine = pool.get('staff.loan.line')
        LoanLine.validate_line(lines)

    @classmethod
    def _clean_payments(cls, loans):
        to_write = []
        for loan in loans:
            to_remove = []
            reconciliations = [l.reconciliation for l in loan.lines_to_pay]
            for payment_line in loan.payment_lines:
                if payment_line.reconciliation not in reconciliations:
                    to_remove.append(payment_line.id)
            if to_remove:
                to_write.append([loan])
                to_write.append({
                        'payment_lines': [('remove', to_remove)],
                        })
        if to_write:
            cls.write(*to_write)

    @classmethod
    def add_payment_lines(cls, payments, origin):
        """
            Add value lines to the key loan from the payment dictionary.
            [{
                instance: [payments]
            }]
        """
        Line = Pool().get('staff.loan.line')
        to_write = []
        paid_lines = []
        for loan, lines in payments.items():
            paid_lines.extend([r.origin for ln in lines for r in ln.reconciliation.lines if r != ln])
            if loan.state == 'paid':
                raise AccessError(
                    gettext('staff_loan'
                        '.msg_loan_payment_lines_add_remove_paid',
                        loan=loan.rec_name))
            to_write.append([loan])
            to_write.append({'payment_lines': [('add', lines)]})
        if to_write:
            cls.write(*to_write)
        if paid_lines:
            Line.write(paid_lines, {'state': 'paid', 'origin': origin})

    @classmethod
    def remove_payment_lines(cls, lines):
        "Remove payment lines from their loan."
        pool = Pool()
        # Line = pool.get('staff.loan.line')
        PaymentLine = pool.get('staff.loan-account.move.line')

        payments = defaultdict(list)
        ids = list(map(int, lines))
        for sub_ids in grouped_slice(ids):
            payment_lines = PaymentLine.search([
                    ('line', 'in', list(sub_ids)),
                    ])
            for payment_line in payment_lines:
                payments[payment_line.loan].append(payment_line.line)

        to_write = []
        # lines_pending = []
        for loan, lines in payments.items():
            if loan.state == 'paid':
                raise AccessError(
                    gettext('staff_loan'
                        '.msg_loan_payment_lines_add_remove_paid',
                        loan=loan.rec_name))
            to_write.append([loan])
            to_write.append({'payment_lines': [('remove', lines)]})
            # lines_pending.extend(lines)
        if to_write:
            cls.write(*to_write)
        # if lines_pending:
        #     Line.write(lines_pending, {'state': 'pending'})

    @classmethod
    def get_lines_to_pay(cls, loans, name):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        line = MoveLine.__table__()
        loan = cls.__table__()
        cursor = Transaction().connection.cursor()

        lines = defaultdict(list)
        for sub_ids in grouped_slice(loans):
            red_sql = reduce_ids(loan.id, sub_ids)
            cursor.execute(*loan.join(line,
                condition=((loan.account_move == line.move)
                    & (loan.account == line.account))).select(
                        loan.id, line.id,
                        where=red_sql,
                        order_by=(loan.id, line.maturity_date.nulls_last)))
            for loan_id, line_id in cursor:
                lines[loan_id].append(line_id)
        return lines

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    @reset_employee('approved_by', 'processed_by')
    def draft(cls, loans):
        pass

    @classmethod
    @ModelView.button
    def calculate(cls, loans):
        for loan in loans:
            if loan.number_instalment and loan.amount and loan.currency and \
                loan.first_pay_date and loan.days_of_month:
                if loan.lines:
                    pool = Pool()
                    Loanline = pool.get('staff.loan.line')
                    Loanline.delete(loan.lines)
                loan.calculate_loan()

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    @set_employee('approved_by')
    def approve(cls, loans):
        for loan in loans:
            if not loan.lines:
                continue
            if not loan.check_loan_approve():
                raise AccessError(gettext('staff_loan.msg_error_loan_payment_plan'))
            if not loan.number:
                loan.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('processed')
    @set_employee('processed_by')
    def process(cls, loans):
        for loan in loans:
            pool = Pool()
            Loanline = pool.get('staff.loan.line')
            lines = [l for l in loan.lines]
            Loanline.write(lines, {'state': 'pending'})

    @classmethod
    @ModelView.button
    @Workflow.transition('posted')
    def post(cls, loans):
        cls._post(loans)

    @classmethod
    def _post(cls, loans):
        pool = Pool()
        Move = pool.get('account.move')
        loans = [l for l in loans if l.type != 'external']
        moves = []
        for loan in loans:
            move = loan.get_move()
            if move != loan.account_move:
                loan.account_move = move
                moves.append(move)
            if loan.state != 'posted':
                loan.state = 'posted'
        if moves:
            Move.save(moves)
        cls.save(loans)
        Move.post([i.account_move for i in loans if i.account_move.state != 'posted'])
        reconciled = []
        for loan in loans:
            if loan.reconciled:
                reconciled.append(loan)
        if reconciled:
            cls.__queue__.refresh(reconciled)

    @classmethod
    def delete(cls, loans):
        cls.cancel(loans)
        for loan in loans:
            if loan.state != 'cancelled':
                raise AccessError(
                    gettext('staff_loan.msg_loan_delete_cancel',
                        loan=loan.rec_name))
            if loan.number:
                raise AccessError(
                    gettext('staff_loan.msg_loan_delete_numbered',
                        loan=loan.rec_name))
        super(Loan, cls).delete(loans)

    def check_loan_approve(self):
        amount = self.amount
        amount_lines = sum([line.amount for line in self.lines])
        if self.company.currency.is_zero(amount - amount_lines):
            return True
        return False

    def get_move(self):
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        MoveLine = pool.get('account.move.line')
        # Date = pool.get('ir.date')
        period_id = Period.find(self.company.id, date=self.date_effective)
        move_lines = []
        total = Decimal('0.0')
        for line in self.lines:
            move_lines += line.get_move_lines()
            total += line.amount
        line = MoveLine()
        line.amount_second_currency = None
        line.second_currency = None
        line.debit, line.credit = 0, total
        line.account = self.payment_mode.account.id
        if line.account.party_required:
            line.party = self.party.id
        line.description = self.description
        move_lines += [line]

        move = Move()
        move.journal = self.payment_mode.journal.id
        move.period = period_id
        move.date = self.date_effective
        move.origin = self
        move.company = self.company
        move.lines = move_lines
        return move

    def set_number(self):
        pool = Pool()
        Configuration = pool.get('loan.configuration')
        configuration = Configuration(1)
        company = Transaction().context.get('company')
        sequence = configuration.get_multivalue('staff_loan_sequence',
                    company=company)
        if not sequence:
            raise LoanMissingSequence(gettext('staff_loan.msg_sequence_missing'))
        seq = sequence.get()
        self.write([self], {'number': seq})

    def calculate_loan(self):
        pool = Pool()
        Loanline = pool.get('staff.loan.line')
        line_to_create = []
        amount_balance = self.amount
        instalment = amount_balance / Decimal(self.number_instalment)
        instalment = Decimal(instalment).quantize(
            Decimal(1) / 10 ** self.__class__.amount.digits[1])
        days_of_month = sorted(set(d.day_of_month for d in self.days_of_month))
        month = self.first_pay_date
        first_month = self.first_pay_date
        if first_month.day not in days_of_month:
            raise AccessError(gettext(
                    'staff_loan.msg_bad_first_pay_date', date=first_month, days=str(days_of_month)))
        while amount_balance > 0:
            if amount_balance != self.amount:
                month = month + relativedelta(months=1)
            if amount_balance <= 0:
                break
            for d in days_of_month:
                try:
                    date_ = date(month.year, month.month, d)
                except:
                    _, last_day = calendar.monthrange(month.year, month.month)
                    date_ = date(month.year, month.month, last_day)
                if first_month > date_:
                    continue
                if amount_balance >= instalment:
                    amount_balance -= instalment
                else:
                    instalment = amount_balance
                    amount_balance -= instalment

                line_to_create.append({
                    'loan': self.id,
                    'maturity_date': date_,
                    'amount': instalment,
                    'state': 'draft',
                    })

                if amount_balance <= 0:
                    break
        Loanline.create(line_to_create)

    def get_reconciled(self, name):
        def get_reconciliation(line):
            if line.reconciliation and line.reconciliation.delegate_to:
                return get_reconciliation(line.reconciliation.delegate_to)
            else:
                return line.reconciliation
        reconciliations = list(map(get_reconciliation, self.lines_to_pay))
        if not reconciliations or not all(reconciliations):
            return None
        else:
            return max(r.date for r in reconciliations)

    @classmethod
    def get_amount_to_pay(cls, loans, name):
        pool = Pool()
        Currency = pool.get('currency.currency')
        # Date = pool.get('ir.date')

        # today = Date.today()
        res = dict((x.id, Decimal(0)) for x in loans)
        for loan in loans:
            if loan.state != 'posted':
                continue
            amount = Decimal(0)
            # amount_currency = Decimal(0)
            for line in loan.lines_to_pay:
                if line.reconciliation:
                    continue
                # if (name == 'amount_to_pay_today'
                #         and (not line.maturity_date
                #             or line.maturity_date > today)):
                #     continue
                # else:
                amount += line.debit - line.credit
            for line in loan.payment_lines:
                if line.reconciliation:
                    continue
                # if (line.second_currency
                #         and line.second_currency == loan.currency):
                #     amount_currency += line.amount_second_currency
                # else:
                amount += line.debit - line.credit
            if amount != Decimal(0):
                pass
                # with Transaction().set_context(date=loan.currency_date):
                #     amount_currency += Currency.compute(
                #         loan.company.currency, amount, loan.currency)
            res[loan.id] = amount
        return res


class LoanLine(ModelSQL, ModelView):
    "Loan Line"
    __name__ = 'staff.loan.line'

    loan = fields.Many2One('staff.loan', 'Loan',
        ondelete='CASCADE', required=True)
    amount = fields.Numeric('Total Amount', digits=(16, 2))
    maturity_date = fields.Date('Maturity Date',
        required=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('pending', 'Pending'),
        ('partial', 'Partial'),
        ('paid', 'Paid'),
    ], 'State', states={
        'required': True,
    })
    state_string = state.translated('state')
    description = fields.Char('Description')
    currency = fields.Function(fields.Many2One(
        'currency.currency', "Currency"), 'on_change_with_currency')

    origin = fields.Reference("Origin", selection='get_origin')

    @classmethod
    def __setup__(cls):
        super(LoanLine, cls).__setup__()
        cls._order.insert(0, ('maturity_date', 'ASC'))

    @classmethod
    def default_state():
        return 'draft'

    @fields.depends('loan', '_parent_loan.currency')
    def on_change_with_currency(self, name=None):
        if self.loan:
            return self.loan.currency.id

    # @fields.depends('state', 'origin')
    # def on_change_with_state(self, name=None):
    #     state = self.state
    #     if not self.origin and self.state == 'paid':
    #         state = 'pending'
    #     elif self.origin and self.state != 'paid':
    #         state = 'paid'
    #     return state

    def get_move_lines(self):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        line = MoveLine()
        amount = self.amount
        line.amount_second_currency = None
        line.second_currency = None
        line.debit, line.credit = amount, 0
        line.account = self.loan.account
        if line.account.party_required:
            line.party = self.loan.party.id
        line.description = self.loan.description
        line.origin = self
        line.maturity_date = self.maturity_date
        return [line]

    @classmethod
    def _get_origin(cls):
        "Return list of Model names for origin Reference"
        return ['staff.loan.line', 'account.voucher']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @classmethod
    def write(cls, *args):
        super(LoanLine, cls).write(*args)
        actions = iter(args)
        all_lines = []
        for lines, values in zip(actions, actions, strict=False):
            all_lines.extend(lines)
        cls.validate_line(all_lines)

    @classmethod
    def validate_line(cls, lines):
        line = cls.__table__()
        cursor = Transaction().connection.cursor()
        pending_lines = []
        paid_lines = []
        for m in lines:
            if not m.origin and m.state == 'paid':
                pending_lines.append(m.id)
            elif m.origin and m.state in ('pending', 'partial'):
                paid_lines.append(m.id)
        for move_ids, state in (
                (pending_lines, 'pending'),
                (paid_lines, 'paid'),
                ):
            if move_ids:
                for sub_ids in grouped_slice(move_ids):
                    red_sql = reduce_ids(line.id, sub_ids)
                    # Use SQL to prevent double validate loop
                    cursor.execute(*line.update(
                            columns=[line.state],
                            values=[state],
                            where=red_sql))


class LoanPaymentLine(ModelSQL):
    "Loan - Payment Line"
    __name__ = 'staff.loan-account.move.line'
    loan = fields.Many2One('staff.loan', 'Loan', ondelete='CASCADE',
            required=True)
    loan_account = fields.Function(
        fields.Many2One('account.account', "Loan Account"),
        'get_loan')
    loan_party = fields.Function(
        fields.Many2One('party.party', "Loan Party"), 'get_loan')
    line = fields.Many2One(
        'account.move.line', 'Payment Line', ondelete='CASCADE',
        required=True,
        domain=[
            ('account', '=', Eval('loan_account')),
            ('party', '=', Eval('loan_party')),
            ],
        depends=['loan_account', 'loan_party'])

    @classmethod
    def __setup__(cls):
        super(LoanPaymentLine, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('line_unique', Unique(t, t.line),
                'staff_loan.msg_loan_payment_line_unique'),
            ]

    @classmethod
    def get_loan(cls, records, names):
        result = {}
        for name in names:
            result[name] = {}
        loan_account = 'loan_account' in result
        loan_party = 'loan_party' in result
        for record in records:
            if loan_account:
                result['loan_account'][record.id] = (
                    record.loan.account.id)
            if loan_party:
                if record.loan.account.party_required:
                    party = record.loan.party.id
                else:
                    party = None
                result['loan_party'][record.id] = party
        return result


# class DayPaymentLoan(ModelSQL, ModelView):
#     "Day Payment Loan"
#     __name__ = 'staff_loan.day_payment.loan'

#     day_of_month = fields.Many2One('staff_loan.day_payment', 'Day of Month')


class DayPayment(ModelSQL, ModelView):
    "Day Payment"
    __name__ = 'staff_loan.day_payment'
    # _rec_name = 'day_of_month'

    loan = fields.Many2One('staff.loan', 'Loan')
    day_of_month = fields.Integer('Day of month')


class LoanReport(Report):
    __name__ = 'staff.loan'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        Date = pool.get('ir.date')
        Company = pool.get('company.company')
        company = Transaction().context.get('company')
        report_context = super().get_context(records, header, data)
        report_context['today'] = Date.today()
        report_context['company'] = Company(company)
        return report_context
